console.log("Hello, world!"); 

function appendData(data, id) { 
	var mainContainer = document.getElementById(id); 
	mainContainer.innerHTML = data; 
}


var ayah1 = fetch("http://ptquran-haghiri75.fandogh.cloud/ar/surah/12/ayah/2")
		.then(response => response.json())
		.then(data => appendData(data, "yusuf-2-ar"))
		.catch(error => console.log(error)); 

var ayah2 = fetch("http://ptquran-haghiri75.fandogh.cloud/fa/surah/12/ayah/2")
		.then(response => response.json())
		.then(data => appendData(data, "yusuf-2-fa")); 

var al_kursi_arabic = fetch("http://ptquran-haghiri75.fandogh.cloud/ar/surah/2/ayah/255")
			.then(response => response.json())
			.then(data => appendData(data, "al-kursi"));

var al_kursi_farsi = fetch("http://ptquran-haghiri75.fandogh.cloud/fa/surah/2/ayah/255")
			.then(response => response.json())
			.then(data => appendData(data, "al-kursi-fa"));
